import 'dart:io';

import 'package:admin_page/Locations/main_screen_locations.dart';
import 'package:admin_page/provider/orderprovider.dart';
import 'package:admin_page/provider/productprovider.dart';
import 'package:admin_page/provider/requestprovider.dart';
import 'package:admin_page/provider/userprovider.dart';
import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:url_strategy/url_strategy.dart';

class MyHttpOverrides extends HttpOverrides{
  @override
  HttpClient createHttpClient(SecurityContext? context){
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }
}

void main() async {
  HttpOverrides.global = MyHttpOverrides();
  setPathUrlStrategy();



  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  final routerDelegate = BeamerDelegate(
    transitionDelegate: const NoAnimationTransitionDelegate(),
    locationBuilder: (routeInformation, _) => HomeLocation(routeInformation),
  );

  @override
  Widget build(BuildContext context) {
    return
      MultiProvider(
          providers: [
            ChangeNotifierProvider(create: (_) => ShowProvider()),
            ChangeNotifierProvider(create: (_) => ProductProvider()),
            ChangeNotifierProvider(create: (_) => BillProvider()),
            ChangeNotifierProvider(create: (_) => UserProvider()),
            ChangeNotifierProvider(create: (_) => RequestProvider()),
          ],
          child: MaterialApp.router(
            debugShowCheckedModeBanner: false,
            routeInformationParser: BeamerParser(),
            routerDelegate: routerDelegate,
          )
      );
  }
}
class ShowProvider extends ChangeNotifier {
  bool _counter = false;
  bool get counter => _counter;
  void show() {
    _counter = true ;
    notifyListeners();
  }
  void off() {
    _counter = false ;
    notifyListeners();
  }
}