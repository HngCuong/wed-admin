class Post {
  int? pageNumber;
  int? pageSize;
  int? totalNumberOfPages;
  int? totalNumberOfRecords;
  List<Bill>? resultss;

  Post(
      {this.pageNumber,
        this.pageSize,
        this.totalNumberOfPages,
        this.totalNumberOfRecords,
        this.resultss});

  Post.fromJson(Map<String, dynamic> json) {
    pageNumber = json['pageNumber'];
    pageSize = json['pageSize'];
    totalNumberOfPages = json['totalNumberOfPages'];
    totalNumberOfRecords = json['totalNumberOfRecords'];
    if (json['results'] != null) {
      resultss = <Bill>[];
      json['results'].forEach((v) {
        resultss!.add(new Bill.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['pageNumber'] = this.pageNumber;
    data['pageSize'] = this.pageSize;
    data['totalNumberOfPages'] = this.totalNumberOfPages;
    data['totalNumberOfRecords'] = this.totalNumberOfRecords;
    if (this.resultss != null) {
      data['resultss'] = this.resultss!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Bill {
  int? id;
  String? orderName;
  String? checkInDate;
  int? totalAmount;
  int? shippingFee;
  int? finalAmount;
  int? orderStatus;
  String? deliveryPhone;
  int? orderType;
  int? timeSlotId;
  int? roomId;
  String? roomNumber;
  int? supplierStoreId;
  String? storeName;
  CustomerInfo? customerInfo;
  List<BillDetail>? orderDetails;

  Bill(
      {this.id,
        this.orderName,
        this.checkInDate,
        this.totalAmount,
        this.shippingFee,
        this.finalAmount,
        this.orderStatus,
        this.deliveryPhone,
        this.orderType,
        this.timeSlotId,
        this.roomId,
        this.roomNumber,
        this.supplierStoreId,
        this.storeName,
        this.customerInfo,
        this.orderDetails});

  Bill.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    orderName = json['orderName'];
    checkInDate = json['checkInDate'];
    totalAmount = json['totalAmount'];
    shippingFee = json['shippingFee'];
    finalAmount = json['finalAmount'];
    orderStatus = json['orderStatus'];
    deliveryPhone = json['deliveryPhone'];
    orderType = json['orderType'];
    timeSlotId = json['timeSlotId'];
    roomId = json['roomId'];
    roomNumber = json['roomNumber'];
    supplierStoreId = json['supplierStoreId'];
    storeName = json['storeName'];
    customerInfo = json['customerInfo'] != null
        ? new CustomerInfo.fromJson(json['customerInfo'])
        : null;
    if (json['orderDetails'] != null) {
      orderDetails = <BillDetail>[];
      json['orderDetails'].forEach((v) {
        orderDetails!.add(new BillDetail.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['orderName'] = this.orderName;
    data['checkInDate'] = this.checkInDate;
    data['totalAmount'] = this.totalAmount;
    data['shippingFee'] = this.shippingFee;
    data['finalAmount'] = this.finalAmount;
    data['orderStatus'] = this.orderStatus;
    data['deliveryPhone'] = this.deliveryPhone;
    data['orderType'] = this.orderType;
    data['timeSlotId'] = this.timeSlotId;
    data['roomId'] = this.roomId;
    data['roomNumber'] = this.roomNumber;
    data['supplierStoreId'] = this.supplierStoreId;
    data['storeName'] = this.storeName;
    if (this.customerInfo != null) {
      data['customerInfo'] = this.customerInfo!.toJson();
    }
    if (this.orderDetails != null) {
      data['orderDetails'] = this.orderDetails!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class CustomerInfo {
  int? id;
  String? name;
  String? phone;
  String? email;
  String? imageUrl;

  CustomerInfo({this.id, this.name, this.phone, this.email, this.imageUrl});

  CustomerInfo.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    phone = json['phone'];
    email = json['email'];
    imageUrl = json['imageUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name'] = this.name;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['imageUrl'] = this.imageUrl;
    return data;
  }
}

class BillDetail {
  String? productName;
  String? image;
  int? quantity;
  int? finalAmount;
  int? status;
  int? productInMenuId;

  BillDetail(
      {this.productName,
        this.image,
        this.quantity,
        this.finalAmount,
        this.status,
        this.productInMenuId});

  BillDetail.fromJson(Map<String, dynamic> json) {
    productName = json['productName'];
    image = json['image'];
    quantity = json['quantity'];
    finalAmount = json['finalAmount'];
    status = json['status'];
    productInMenuId = json['productInMenuId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['productName'] = this.productName;
    data['image'] = this.image;
    data['quantity'] = this.quantity;
    data['finalAmount'] = this.finalAmount;
    data['status'] = this.status;
    data['productInMenuId'] = this.productInMenuId;
    return data;
  }
}