
class BlogItem {
  String? id, date, title, description, image;

  BlogItem({this.id,this.date, this.title, this.description, this.image});

  BlogItem.fromJson(Map<String, dynamic> json){
    id = json['id'].toString();
    date = json['date'];
    title = json['tittle'];
    description = json['long'];
    image = json['short'];
  }

}

List<BlogItem> blogPosts = [
  BlogItem(
    id: "1",
    date: "13 October 2023",
    image: "assets/images/0.png",
    title: "How Ireland’s biggest bank executed a comp lete security redesign",
    description:
        "Necklace",
  ),
  BlogItem(
    id: "2",
    date: "13 October 2023",
    image: "assets/images/1.png",
    title: "5 Examples of Web Motion Design That Really Catch Your Eye",
    description:
"Necklace"  ),
  BlogItem(
    id: "3",
    date: "13 October 2023",
    image: "assets/images/2.png",
    title: "The Principles of Dark UI Design",
    description:
"Necklace"  ),
  BlogItem(
    id: "4",
    date: "13 October 2023",
    image: "assets/images/2.png",
    title: "The Principles of Dark UI Design",
    description:
"Necklace"  ),
  BlogItem(
    id: "5",
    date: "13 October 2023",
    image: "assets/images/2.png",
    title: "The Principles of Dark UI Design",
    description:
"Necklace"  ),
];
