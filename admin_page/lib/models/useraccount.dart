class User {
  int? pageNumber;
  int? pageSize;
  int? totalNumberOfPages;
  int? totalNumberOfRecords;
  List<Acc>? results;

  User(
      {this.pageNumber,
        this.pageSize,
        this.totalNumberOfPages,
        this.totalNumberOfRecords,
        this.results});

  User.fromJson(Map<String, dynamic> json) {
    pageNumber = json['pageNumber'];
    pageSize = json['pageSize'];
    totalNumberOfPages = json['totalNumberOfPages'];
    totalNumberOfRecords = json['totalNumberOfRecords'];
    if (json['results'] != null) {
      results = <Acc>[];
      json['results'].forEach((v) {
        results!.add(new Acc.fromJson(v));
      });
    }
  }

}

class Acc {
  int? id;
  String? name;
  String? phone;
  String? email;
  String? imageUrl;

  Acc({this.id, this.name, this.phone, this.email, this.imageUrl});

  Acc.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    phone = json['phone'];
    email = json['email'];
    imageUrl = json['imageUrl'];
  }

}