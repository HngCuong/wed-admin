
class Order {
  int? totalAmount;
  String? deliveryPhone;
  int orderType = 1;
  int timeSlotId = 2;
  int roomId = 1;
  int customerId = 1;
  List<OrderDetails>? orderDetails;
  Order(
      {this.totalAmount,
        this.deliveryPhone,
        this.orderDetails});

  Order.fromJson(Map<String, dynamic> json) {
    totalAmount = json['totalAmount'];
    deliveryPhone = json['deliveryPhone'];
    orderType = json['orderType'];
    timeSlotId = json['timeSlotId'];
    roomId = json['roomId'];
    customerId = json['customerId'];
    deliveryPhone=json['deliveryPhone'];
    if (json['orderDetails'] != null) {
      orderDetails = <OrderDetails>[];
      json['orderDetails'].forEach((v) {
        orderDetails!.add(new OrderDetails.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['totalAmount'] = this.totalAmount ;
    data['deliveryPhone'] = this.deliveryPhone;
    data['orderType'] = this.orderType;
    data['timeSlotId'] = this.timeSlotId;
    data['roomId'] = this.roomId;
    data['customerId'] = this.customerId;
    if (this.orderDetails != null) {
      data['orderDetails'] = this.orderDetails!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class OrderDetails {
  int? productInMenuId;
  int timeSlotId = 2 ;
  int? quantity;
  int? finalAmount ;
  int supplierStoreId = 1;

  OrderDetails(
      {this.productInMenuId,
        this.quantity,
        this.finalAmount,
      });


  OrderDetails.fromJson(Map<String, dynamic> json) {
    productInMenuId = json['productInMenuId'];
    timeSlotId = json['timeSlotId'];
    quantity = json['quantity'];
    finalAmount = json['finalAmount'];
    supplierStoreId = json['supplierStoreId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['productInMenuId'] = this.productInMenuId;
    data['timeSlotId'] = this.timeSlotId;
    data['quantity'] = this.quantity;
    data['finalAmount'] = this.finalAmount;
    data['supplierStoreId'] = this.supplierStoreId;
    return data;
  }
}