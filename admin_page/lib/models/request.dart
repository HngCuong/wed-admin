class Request {
  int? pageNumber;
  int? pageSize;
  int? totalNumberOfPages;
  int? totalNumberOfRecords;
  List<RequestUser>? results;

  Request(
      {this.pageNumber,
        this.pageSize,
        this.totalNumberOfPages,
        this.totalNumberOfRecords,
        this.results});

  Request.fromJson(Map<String, dynamic> json) {
    pageNumber = json['pageNumber'];
    pageSize = json['pageSize'];
    totalNumberOfPages = json['totalNumberOfPages'];
    totalNumberOfRecords = json['totalNumberOfRecords'];
    if (json['results'] != null) {
      results = <RequestUser>[];
      json['results'].forEach((v) {
        results!.add(new RequestUser.fromJson(v));
      });
    }
  }

}

class RequestUser {
  int? id;
  String? name;
  String? phone;
  String? adress;
  String? date;
  String? material;
  String? color;
  String? prices;
  String? description;
  String? img;

  RequestUser(
      {this.id,
        this.name,
        this.phone,
        this.adress,
        this.date,
        this.material,
        this.color,
        this.prices,
        this.description,
        this.img});

  RequestUser.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    phone = json['phone'];
    adress = json['adress'];
    date = json['date'];
    material = json['material'];
    color = json['color'];
    prices = json['prices'];
    description = json['description'];
    img = json['img'];
  }

}