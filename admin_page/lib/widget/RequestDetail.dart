import 'package:admin_page/api/apis.dart';
import 'package:admin_page/models/product.dart';
import 'package:admin_page/models/request.dart';
import 'package:admin_page/provider/productprovider.dart';
import 'package:flutter/material.dart';
import 'package:image_network/image_network.dart';

class RequestDetail extends StatefulWidget {
  final VoidCallback updateScreenCallback;
  final RequestUser request ;
  RequestDetail({super.key, required this.updateScreenCallback, required this.request});

  @override
  State<RequestDetail> createState() => _SearchDialogState();
}

class _SearchDialogState extends State<RequestDetail> {
  bool create = false;
  TextEditingController nameController = TextEditingController() ;
  TextEditingController priceController = TextEditingController();
  TextEditingController imgController = TextEditingController();
  TextEditingController rateController = TextEditingController();

  void _onButtonPressed() {
    widget.updateScreenCallback();
  }

  @override
  Widget build(BuildContext context) {

    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      elevation: 0.0,
      backgroundColor: Colors.white,
      child: contentBox(context),
    );
  }

  Widget contentBox(BuildContext context) {
    return Container(
      height: 600,
      width: 400,
      child: Column(
        children: [
          ImageNetwork(
            image:'${widget.request.img}', height: 250, width: 250 ,
          ),
          SizedBox(height: 5,),
          Text(
            "${widget.request.prices} VNĐ",
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 5,),
          Text(
            "${widget.request.color} ",
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.bold,
            ),
          ),
          SizedBox(height: 5,),
          Text(
            "${widget.request.material}",
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.bold,
            ),
          ),

          SizedBox(height: 5,),
          Text(
            "${widget.request.description}",
            style: TextStyle(
              fontSize: 12,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}
