
import 'package:admin_page/constants/style.dart';
import 'package:admin_page/models/blog.dart';
import 'package:admin_page/provider/orderprovider.dart';
import 'package:admin_page/widget/custom_text.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class BlogTable extends StatefulWidget {
  const BlogTable({super.key});
  @override
  State<BlogTable> createState() => _AvailableDriversTableState();
}
class _AvailableDriversTableState extends State<BlogTable> {
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<BillProvider>(context, listen: false);
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: active.withOpacity(.4), width: .5),
        boxShadow: [BoxShadow(offset: const Offset(0, 6), color: lightGrey.withOpacity(.1), blurRadius: 12)],
        borderRadius: BorderRadius.circular(8),
      ),
      padding: const EdgeInsets.all(16),
      margin: const EdgeInsets.only(bottom: 30),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              SizedBox(
                width: 10,
              ),
              CustomText(
                text: "Available Products",
                color: lightGrey,
                weight: FontWeight.bold,
              ),
              SizedBox(width: 20,),
            ],
          ),
          SizedBox(
            height: (56 * 7) + 40,
            child: DataTable2(
              columnSpacing: 12,
              dataRowHeight: 56,
              headingRowHeight: 40,
              horizontalMargin: 12,
              minWidth: 600,
              columns: const [
                DataColumn2(
                  label: Text("ID"),
                  size: ColumnSize.L,
                ),
                DataColumn(
                  label: Text('Name'),
                ),
                DataColumn(
                  label: Text('DatePublic'),
                ),
                DataColumn(
                  label: Text('DateUpdate'),
                ),
                DataColumn(
                  label: Text("Category"),
                ),
              ],
              rows: List<DataRow>.generate(
                5,
                    (index) => DataRow(
                  cells: [
                    DataCell(CustomText(text: "${blogPosts[index].id}")),
                    DataCell(CustomText(text: "${blogPosts[index].title}")),
                    DataCell(
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(Icons.star, color: Colors.deepOrange, size: 18),
                          SizedBox(width: 5),
                          CustomText(text: "${blogPosts[index].date}"),
                        ],
                      ),
                    ),
                    DataCell(CustomText(text: "${blogPosts[index].date}")),
                    DataCell(CustomText(text: "${blogPosts[index].description}")),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
