import 'dart:async';
import 'package:admin_page/api/apis.dart';
import 'package:admin_page/constants/style.dart';
import 'package:admin_page/models/product.dart';
import 'package:admin_page/provider/productprovider.dart';
import 'package:admin_page/widget/createproduct.dart';
import 'package:admin_page/widget/custom_text.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

/// Example without datasource
class AvailableDriversTable extends StatefulWidget {
  const AvailableDriversTable({super.key});

  @override
  State<AvailableDriversTable> createState() => _AvailableDriversTableState();
}

class _AvailableDriversTableState extends State<AvailableDriversTable> {
  List<Product> list = [];

  @override
  void updateScreen() {
  }
  void callAgain(){
    setState(() {
    });
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<ProductProvider>(context, listen: false);
    provider.addData();
    Timer(Duration(milliseconds: 500), () {
      list.clear();
      for(var item in provider.listProduct){
        list.add(item);
      }
      setState(() {

      });
      print(list.length);
      print("Da chay provider");
    }
    );
    void showSearchDialog(BuildContext context) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return CreateProduct(updateScreenCallback: updateScreen);
        },
      );
    }
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: active.withOpacity(.4), width: .5),
        boxShadow: [BoxShadow(offset: const Offset(0, 6), color: lightGrey.withOpacity(.1), blurRadius: 12)],
        borderRadius: BorderRadius.circular(8),
      ),
      padding: const EdgeInsets.all(16),
      margin: const EdgeInsets.only(bottom: 30),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              SizedBox(
                width: 10,
              ),
              CustomText(
                text: "Available Products",
                color: lightGrey,
                weight: FontWeight.bold,
              ),
              SizedBox(width: 20,),
              InkWell(
                onTap: () {
                  showSearchDialog(context);
                },
                borderRadius: BorderRadius.circular(10.0),
                splashColor: Colors.blue,
                child: Container(
                  height: 50,
                  width: 200,
                  padding: EdgeInsets.all(16.0),
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(10.0),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 2,
                        blurRadius: 5,
                        offset: Offset(0, 3),
                      ),
                    ],
                  ),
                  child: Center(
                    child: Text(
                      textAlign: TextAlign.center,
                      'Create Product',
                      style: TextStyle(fontSize: 15.0, fontWeight: FontWeight.bold,color: Colors.blue),
                    ),
                  ),
                ),
              ),
            ],
          ),
          SizedBox(
            height: (56 * 7) + 40,
            child: DataTable2(
              columnSpacing: 12,
              dataRowHeight: 56,
              headingRowHeight: 40,
              horizontalMargin: 12,
              minWidth: 600,
              columns: const [
                DataColumn2(
                  label: Text("Name"),
                  size: ColumnSize.L,
                ),
                DataColumn(
                  label: Text('Price'),
                ),
                DataColumn(
                  label: Text('Rating'),
                ),
                DataColumn(
                  label: Text('Action'),
                ),
              ],
              rows: List<DataRow>.generate(
                list.length,
                    (index) => DataRow(
                  cells: [
                    DataCell(CustomText(text: "${list[index].name}")),
                    DataCell(CustomText(text: "${list[index].price}")),
                    DataCell(
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(Icons.star, color: Colors.deepOrange, size: 18),
                          SizedBox(width: 5),
                          CustomText(text: "${list[index].code}"),
                        ],
                      ),
                    ),
                    DataCell(
                      Container(
                        decoration: BoxDecoration(
                          color: light,
                          borderRadius: BorderRadius.circular(20),
                          border: Border.all(color: list[index].status == 1 ? no : active, width: .5),
                        ),
                        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                        child: InkWell(
                          onTap: (){
                            int a = index + 1;
                            fetchStatusProduct(a.toString()).then((value) => {
                              provider.addData()
                            });
                          },
                          child: CustomText(
                            text: "Active/No",
                            color: list[index].status == 1 ? no.withOpacity(.7) :  active.withOpacity(.7),
                            weight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
