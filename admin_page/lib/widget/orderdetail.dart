import 'dart:async';

import 'package:admin_page/api/apis.dart';
import 'package:admin_page/models/bill.dart';
import 'package:admin_page/models/product.dart';
import 'package:admin_page/provider/orderprovider.dart';
import 'package:admin_page/provider/productprovider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OrderDetail extends StatefulWidget {
  final List<BillDetail> obj ;
  final VoidCallback updateScreenCallback;

  OrderDetail({super.key, required this.updateScreenCallback, required this.obj});

  @override
  State<OrderDetail> createState() => _SearchDialogState();
}

class _SearchDialogState extends State<OrderDetail> {
  bool create = false;
  TextEditingController nameController = TextEditingController() ;
  TextEditingController priceController = TextEditingController();
  TextEditingController imgController = TextEditingController();
  TextEditingController rateController = TextEditingController();

  void _onButtonPressed() {
    widget.updateScreenCallback();
  }

  @override
  Widget build(BuildContext context) {

    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      elevation: 0.0,
      backgroundColor: Colors.white,
      child: contentBox(context),
    );
  }
  Widget contentBox(BuildContext context) {

    return Container(
      child: Column(
        children: [
          Container(
            height:500,
            width: 700,
            child: ListView.builder(
                itemCount: widget.obj.length,
                itemBuilder: (context, index) {
                  return Container(
                    padding: EdgeInsets.all(30),
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                    width: 130,
                                    height: 90,
                                    decoration: BoxDecoration(
                                      color: Colors.grey.shade200,
                                      borderRadius: BorderRadius.circular(15),
                                      image: DecorationImage(
                                          image: NetworkImage("https://www.google.com/search?sca_esv=572890011&q=naruto&tbm=isch&source=lnms&sa=X&ved=2ahUKEwi8xN7A7PCBAxWKrlYBHYqoACMQ0pQJegQIDRAB&biw=1280&bih=603&dpr=1.5#imgrc=DWP1Fa2f1YCBaM"),
                                          fit: BoxFit.cover),
                                    )),
                                SizedBox(
                                  width: 30,
                                ),
                                Container(
                                  width: 175,
                                  child: Column(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Text(
                                        "${widget.obj[index].productName}",
                                        style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.w700,
                                        ),
                                      ),
                                      Text(
                                       "${widget.obj[index].finalAmount! * widget.obj[index].quantity!} ",
                                        style: TextStyle(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w700,
                                            color: Colors.grey),
                                      ),
                                      SizedBox(
                                        height: 10,
                                      ),
                                      Row(
                                        children: [
                                          Text(
                                            "Amount",
                                            style: TextStyle(
                                                fontSize: 11,
                                                fontWeight: FontWeight.w600),
                                          ),
                                          SizedBox(
                                            width: 120,
                                          ),
                                          Text(
                                            "x ${widget.obj[index].quantity}",
                                            style: TextStyle(
                                                fontSize: 11,
                                                fontWeight: FontWeight.w600,
                                                color: Colors.grey),
                                          )
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ]),
                        ]),
                  );
                }),
          ),

        ],
      ),
    );
  }
}
