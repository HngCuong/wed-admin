import 'dart:async';
import 'package:admin_page/api/apis.dart';
import 'package:admin_page/constants/style.dart';
import 'package:admin_page/models/bill.dart';
import 'package:admin_page/models/useraccount.dart';
import 'package:admin_page/provider/orderprovider.dart';
import 'package:admin_page/provider/userprovider.dart';
import 'package:admin_page/widget/createproduct.dart';
import 'package:admin_page/widget/custom_text.dart';
import 'package:admin_page/widget/orderdetail.dart';
import 'package:admin_page/widget/statusorder.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

/// Example without datasource
class UserTable extends StatefulWidget {
  const UserTable({super.key});
  @override
  State<UserTable> createState() => _AvailableDriversTableState();
}
class _AvailableDriversTableState extends State<UserTable> {
  List<Acc> list = [];

  @override
  void updateScreen() {

  }
  void callAgain(){
    setState(() {
    });
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<UserProvider>(context, listen: false);
    provider.addData();
    Timer(Duration(milliseconds: 500), () {
      list.clear();
      for (var item in provider.item) {
        list.add(item);
      }
      setState(() {
      });
    }
    );
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: active.withOpacity(.4), width: .5),
        boxShadow: [BoxShadow(offset: const Offset(0, 6), color: lightGrey.withOpacity(.1), blurRadius: 12)],
        borderRadius: BorderRadius.circular(8),
      ),
      padding: const EdgeInsets.all(16),
      margin: const EdgeInsets.only(bottom: 30),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              SizedBox(
                width: 10,
              ),
              CustomText(
                text: "Available Products",
                color: lightGrey,
                weight: FontWeight.bold,
              ),
              SizedBox(width: 20,),
            ],
          ),
          SizedBox(
            height: (56 * 7) + 40,
            child: DataTable2(
              columnSpacing: 12,
              dataRowHeight: 56,
              headingRowHeight: 40,
              horizontalMargin: 12,
              minWidth: 600,
              columns: const [
                DataColumn2(
                  label: Text("ID"),
                  size: ColumnSize.L,
                ),
                DataColumn(
                  label: Text('Name'),
                ),
                DataColumn(
                  label: Text('Pass'),
                ),
              ],
              rows: List<DataRow>.generate(
                list.length,
                    (index) => DataRow(
                  cells: [
                    DataCell(CustomText(text: "${list[index].id}")),
                    DataCell(CustomText(text: "${list[index].name}")),
                    DataCell(
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(Icons.star, color: Colors.deepOrange, size: 18),
                          SizedBox(width: 5),
                          CustomText(text: "${list[index].imageUrl}"),
                        ],
                      ),
                    ),

                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
