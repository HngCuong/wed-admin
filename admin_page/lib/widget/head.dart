import 'dart:async';
import 'package:admin_page/models/bill.dart';
import 'package:admin_page/provider/orderprovider.dart';
import 'package:admin_page/widget/info_card.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class Head extends StatefulWidget {
  const Head({super.key});

  @override
  State<Head> createState() => _State();
}
List<Bill> list = [];
int cancel = 0;
int progress = 0;
int check = 0;
void update(){
  cancel = 0;
  progress = 0;
  check = 0;
  for(var item in list){
    if(item.orderStatus == 0){
      progress++;
    }else if (item.orderStatus == 2){
      check++;
    }else if (item.orderStatus == 1)
      cancel++;
  }
}


class _State extends State<Head> {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      var provider = Provider.of<BillProvider>(context, listen: false);
      provider.addData();
      Timer(Duration(milliseconds: 500), () {
        list.clear();
        for (var item in provider.item) {
          list.add(item);
        }
        update();
        setState(() {
        });
      }
      );
    });
  }
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Row(
      children: [
        InfoCard(
          title: "Order in progress",
          value: "${progress}",
          onTap: () {},
          topColor: Colors.orange,
        ),
        SizedBox(
          width: width / 64,
        ),
        InfoCard(
          title: "Not Check Order",
          value: "${check}",
          topColor: Colors.lightGreen,
          onTap: () {},
        ),
        SizedBox(
          width: width / 64,
        ),
        InfoCard(
          title: "Cancelled Order",
          value: "${cancel}",
          topColor: Colors.redAccent,
          onTap: () {},
        ),
        SizedBox(
          width: width / 64,
        ),
        InfoCard(
          title: "Total Order",
          value: "${list.length}",
          onTap: () {},
        ),
      ],
    );
  }
}
