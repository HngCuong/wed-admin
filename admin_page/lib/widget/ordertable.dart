import 'dart:async';
import 'package:admin_page/constants/style.dart';
import 'package:admin_page/models/bill.dart';
import 'package:admin_page/provider/orderprovider.dart';
import 'package:admin_page/widget/custom_text.dart';
import 'package:admin_page/widget/orderdetail.dart';
import 'package:admin_page/widget/statusorder.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

/// Example without datasource
class OrderTable extends StatefulWidget {
  const OrderTable({super.key});
  @override
  State<OrderTable> createState() => _AvailableDriversTableState();
}
class _AvailableDriversTableState extends State<OrderTable> {
  List<Bill> list = [];

  @override
  void updateScreen() {

  }
  void callAgain(){
    setState(() {
    });
  }

  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<BillProvider>(context, listen: false);
    Timer(Duration(milliseconds: 500), () {
      list.clear();
      for (var item in provider.item) {
        list.add(item);
      }
      setState(() {
      });
    }
    );
    void showStatus(BuildContext context,String id) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return StatusOrder(updateScreenCallback: updateScreen, id: id,);
        },
      );
    }
    void showDetail(BuildContext context, List<BillDetail> billdetail) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return OrderDetail(updateScreenCallback: updateScreen,obj: billdetail,);
        },
      );
    }
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: active.withOpacity(.4), width: .5),
        boxShadow: [BoxShadow(offset: const Offset(0, 6), color: lightGrey.withOpacity(.1), blurRadius: 12)],
        borderRadius: BorderRadius.circular(8),
      ),
      padding: const EdgeInsets.all(16),
      margin: const EdgeInsets.only(bottom: 30),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              SizedBox(
                width: 10,
              ),
              CustomText(
                text: "Available Products",
                color: lightGrey,
                weight: FontWeight.bold,
              ),
              SizedBox(width: 20,),
            ],
          ),
          SizedBox(
            height: (56 * 7) + 40,
            child: DataTable2(
              columnSpacing: 12,
              dataRowHeight: 56,
              headingRowHeight: 40,
              horizontalMargin: 12,
              minWidth: 600,
              columns: const [
                DataColumn2(
                  label: Text("OrderName"),
                  size: ColumnSize.L,
                ),
                DataColumn(
                  label: Text('Price'),
                ),
                DataColumn(
                  label: Text('Customer'),
                ),
                DataColumn(
                  label: Text('Status'),
                ),
                DataColumn(
                  label: Text("Detail"),
                ),
              ],
              rows: List<DataRow>.generate(
                list.length,
                    (index) => DataRow(
                  cells: [
                    DataCell(CustomText(text: "${list[index].orderName}")),
                    DataCell(CustomText(text: "${list[index].finalAmount}")),
                    DataCell(
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(Icons.star, color: Colors.deepOrange, size: 18),
                          SizedBox(width: 5),
                          CustomText(text: "${list[index].customerInfo!.name}"),
                        ],
                      ),
                    ),
                    DataCell(
                      Container(
                        decoration: BoxDecoration(
                          color: light,
                          borderRadius: BorderRadius.circular(20),
                          border: Border.all(color:  active, width: .5),
                        ),
                        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                        child: InkWell(
                          onTap: (){
                            showStatus(context,list[index].id.toString());
                          },
                          child: CustomText(
                            text: list[index].orderStatus == 0 ? "Đang giao" : list[index].orderStatus == 1 ? "Cancel" : list[index].orderStatus == 2 ? "Chờ Xác Nhận"  : "Hoàn tất",
                            color:  active.withOpacity(.7),
                            weight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                    DataCell(
                      Container(
                        decoration: BoxDecoration(
                          color: light,
                          borderRadius: BorderRadius.circular(20),
                          border: Border.all(color:  active, width: .5),
                        ),
                        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                        child: InkWell(
                          onTap: (){
                            showDetail(context,list[index].orderDetails!);
                          },
                          child: CustomText(
                            text: "Active/No",
                            color:  active.withOpacity(.7),
                            weight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
