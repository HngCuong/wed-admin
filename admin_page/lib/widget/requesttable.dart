import 'dart:async';
import 'package:admin_page/api/apis.dart';
import 'package:admin_page/constants/style.dart';
import 'package:admin_page/models/bill.dart';
import 'package:admin_page/models/request.dart';
import 'package:admin_page/models/useraccount.dart';
import 'package:admin_page/provider/orderprovider.dart';
import 'package:admin_page/provider/requestprovider.dart';
import 'package:admin_page/provider/userprovider.dart';
import 'package:admin_page/widget/RequestDetail.dart';
import 'package:admin_page/widget/createproduct.dart';
import 'package:admin_page/widget/custom_text.dart';
import 'package:admin_page/widget/orderdetail.dart';
import 'package:admin_page/widget/statusorder.dart';
import 'package:data_table_2/data_table_2.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

/// Example without datasource
class RequestTable extends StatefulWidget {
  const RequestTable({super.key});
  @override
  State<RequestTable> createState() => _AvailableDriversTableState();
}
class _AvailableDriversTableState extends State<RequestTable> {
  List<RequestUser> list = [];

  @override
  void updateScreen() {

  }
  void callAgain(){
    setState(() {
    });
  }
  void showDetail(BuildContext context, RequestUser request) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return RequestDetail(updateScreenCallback: updateScreen, request : request ,);
      },
    );
  }
  @override
  Widget build(BuildContext context) {
    var provider = Provider.of<RequestProvider>(context, listen: false);
    provider.addData();
    Timer(Duration(milliseconds: 500), () {
      list.clear();
      for (var item in provider.item) {
        list.add(item);
      }
      setState(() {
      });
    }
    );
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(color: active.withOpacity(.4), width: .5),
        boxShadow: [BoxShadow(offset: const Offset(0, 6), color: lightGrey.withOpacity(.1), blurRadius: 12)],
        borderRadius: BorderRadius.circular(8),
      ),
      padding: const EdgeInsets.all(16),
      margin: const EdgeInsets.only(bottom: 30),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(
            children: [
              SizedBox(
                width: 10,
              ),
              CustomText(
                text: "Available Products",
                color: lightGrey,
                weight: FontWeight.bold,
              ),
              SizedBox(width: 20,),
            ],
          ),
          SizedBox(
            height: (56 * 7) + 40,
            child: DataTable2(
              columnSpacing: 12,
              dataRowHeight: 56,
              headingRowHeight: 40,
              horizontalMargin: 12,
              minWidth: 600,
              columns: const [
                DataColumn2(
                  label: Text("ID"),
                  size: ColumnSize.L,
                ),
                DataColumn(
                  label: Text('Name'),
                ),
                DataColumn(
                  label: Text('Phone'),
                ),
                DataColumn(
                  label: Text('Date'),
                ),
                DataColumn(
                  label: Text('Detail'),
                ),
              ],
              rows: List<DataRow>.generate(
                list.length,
                    (index) => DataRow(
                  cells: [
                    DataCell(CustomText(text: "${list[index].id}")),
                    DataCell(CustomText(text: "${list[index].name}")),
                    DataCell(
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Icon(Icons.star, color: Colors.deepOrange, size: 18),
                          SizedBox(width: 5),
                          CustomText(text: "${list[index].phone}"),
                        ],
                      ),
                    ),
                    DataCell(CustomText(text: "${list[index].date}")),
                    DataCell(
                      Container(
                        decoration: BoxDecoration(
                          color: light,
                          borderRadius: BorderRadius.circular(20),
                          border: Border.all(color:active, width: .5),
                        ),
                        padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
                        child: InkWell(
                          onTap: (){
                            showDetail(context,list[index]);
                          },
                          child: CustomText(
                            text: "Active/No",
                            color: active.withOpacity(.7),
                            weight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),

                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
