import 'package:admin_page/api/apis.dart';
import 'package:admin_page/models/product.dart';
import 'package:admin_page/provider/orderprovider.dart';
import 'package:admin_page/provider/productprovider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class StatusOrder extends StatefulWidget {
  final VoidCallback updateScreenCallback;
  final String id;
  StatusOrder({super.key, required this.updateScreenCallback, required this.id});

  @override
  State<StatusOrder> createState() => _SearchDialogState();
}

class _SearchDialogState extends State<StatusOrder> {

  void _onButtonPressed() {
    widget.updateScreenCallback();
  }

  @override
  Widget build(BuildContext context) {
    print("Day la id"+ widget.id);
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      elevation: 0.0,
      backgroundColor: Colors.white,
      child: contentBox(context),
    );
  }

  Widget contentBox(BuildContext context) {

    return Container(
      height: 300,
     width: 350,
     child: Center(
       child: Column(
         children: [
           SizedBox(height: 20),
           ElevatedButton(
             onPressed: () async {
               if(await fetchBillStatus0(widget.id)) {
                 var provider = Provider.of<BillProvider>(
                     context, listen: false);
                 provider.addData();
                 setState(() {
                 });
               }
             },
             child: Text(
               'Đang Xử Lí',
               style: TextStyle(fontSize: 20),
             ),
             style: ElevatedButton.styleFrom(
               primary: Colors.yellow, // Màu nền của nút
               onPrimary: Colors.white, // Màu chữ trên nút
               padding: EdgeInsets.symmetric(horizontal: 40, vertical: 10), // Kích thước nút
               shape: RoundedRectangleBorder(
                 borderRadius: BorderRadius.circular(20), // Góc bo của nút
               ),
             ),
           ),
           SizedBox(height: 20), // Khoảng cách giữa các nút
           ElevatedButton(
             onPressed: () async {
               if(await fetchBillStatus1(widget.id)) {
               var provider = Provider.of<BillProvider>(
               context, listen: false);
               provider.addData();
               setState(() {
               });
               }
             },
             child: Text(
               'Cancel',
               style: TextStyle(fontSize: 20),
             ),
             style: ElevatedButton.styleFrom(
               primary: Colors.red, // Màu nền của nút
               onPrimary: Colors.white, // Màu chữ trên nút
               padding: EdgeInsets.symmetric(horizontal: 40, vertical: 10), // Kích thước nút
               shape: RoundedRectangleBorder(
                 borderRadius: BorderRadius.circular(20), // Góc bo của nút
               ),
             ),
           ),
           SizedBox(height: 20), // Khoảng cách giữa các nút
           ElevatedButton(
             onPressed: () async {
               if(await fetchBillStatus2(widget.id)) {
               var provider = Provider.of<BillProvider>(
               context, listen: false);
               provider.addData();
               setState(() {
               });
               }
             },
             child: Text(
               'Chờ Xác Nhận',
               style: TextStyle(fontSize: 20),
             ),
             style: ElevatedButton.styleFrom(
               primary: Colors.grey, // Màu nền của nút
               onPrimary: Colors.white, // Màu chữ trên nút
               padding: EdgeInsets.symmetric(horizontal: 40, vertical: 10), // Kích thước nút
               shape: RoundedRectangleBorder(
                 borderRadius: BorderRadius.circular(20), // Góc bo của nút
               ),
             ),
           ),
           SizedBox(height: 20), // Khoảng cách giữa các nút
           ElevatedButton(
             onPressed: () async {
               if(await fetchBillStatus3(widget.id)) {
               var provider = Provider.of<BillProvider>(
               context, listen: false);
               provider.addData();
               setState(() {
               });
               }
             },
             child: Text(
               'Hoàn Thành',
               style: TextStyle(fontSize: 20),
             ),
             style: ElevatedButton.styleFrom(
               primary: Colors.green, // Màu nền của nút
               onPrimary: Colors.white, // Màu chữ trên nút
               padding: EdgeInsets.symmetric(horizontal: 40, vertical: 10), // Kích thước nút
               shape: RoundedRectangleBorder(
                 borderRadius: BorderRadius.circular(20), // Góc bo của nút
               ),
             ),
           ),
         ],
       ),
     ),
    );
  }
}
