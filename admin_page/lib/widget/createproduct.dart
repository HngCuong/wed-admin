import 'package:admin_page/api/apis.dart';
import 'package:admin_page/models/product.dart';
import 'package:admin_page/provider/productprovider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CreateProduct extends StatefulWidget {
  final VoidCallback updateScreenCallback;

  CreateProduct({super.key, required this.updateScreenCallback});

  @override
  State<CreateProduct> createState() => _SearchDialogState();
}

class _SearchDialogState extends State<CreateProduct> {
  bool create = false;
  TextEditingController nameController = TextEditingController() ;
  TextEditingController priceController = TextEditingController();
  TextEditingController imgController = TextEditingController();
  TextEditingController rateController = TextEditingController();

  void _onButtonPressed() {
    widget.updateScreenCallback();
  }

  @override
  Widget build(BuildContext context) {

    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      elevation: 0.0,
      backgroundColor: Colors.transparent,
      child: contentBox(context),
    );
  }

  Widget contentBox(BuildContext context) {

    return Container(
      padding: EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        color: Colors.white,
        borderRadius: BorderRadius.circular(10.0),
      ),
      child: Container(
        height: 600,
        width: 800,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              'Create Product',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
              ),
            ),
            SizedBox(height: 16.0),
            TextField(
              controller: nameController,
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                hintText: 'Enter Name Product',
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 16.0),
            TextField(
              keyboardType: TextInputType.text,
              decoration: InputDecoration(
                hintText: 'Enter description',
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 16.0),
            TextField(
              keyboardType: TextInputType.text,
              controller: priceController,
              decoration: InputDecoration(
                hintText: 'Enter Price',
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 16.0),
            TextField(
              keyboardType: TextInputType.text,
              controller: rateController,
              decoration: InputDecoration(
                hintText: 'Enter rating',
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 16.0),
            TextField(
              keyboardType: TextInputType.text,
              controller: imgController,
              decoration: InputDecoration(
                hintText: 'Enter Image',
                border: OutlineInputBorder(),
              ),
            ),
            SizedBox(height: 10.0),
            SizedBox(height: 10.0),
            ElevatedButton(
              onPressed: () async {
                ProductCreate a = new ProductCreate(categoryId: 1,
                code: rateController.text ,
                detail: "san pham" ,
                image: imgController.text ,
                name: nameController.text ,
                price: int.parse(priceController.text) ,
                quantity: 100 ,
                status: 0 ,
                supplierStoreId: 1 ,
                );
                if(await createProduct(a)){
                  var provider = Provider.of<ProductProvider>(context, listen: false);
                  provider.addData();
                  setState(() {
                    create = true;
                  });
                }

              },
              child: create ? Icon(
                Icons.check_circle,
                color: Colors.green,
                size: 48.0,
              ) : Text('Create Product'),
            ),
          ],
        ),
      ),
    );
  }
}
