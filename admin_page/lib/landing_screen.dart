import 'package:admin_page/provider/productprovider.dart';
import 'package:admin_page/screens/blogscreen.dart';
import 'package:admin_page/screens/orderscreen.dart';
import 'package:admin_page/screens/productscreen.dart';
import 'package:admin_page/screens/requestscreen.dart';
import 'package:admin_page/screens/userscreen.dart';
import 'package:admin_page/widget/available_drivers_table.dart';
import 'package:admin_page/widget/bar_chart.dart';
import 'package:admin_page/widget/head.dart';
import 'package:admin_page/widget/ordertable.dart';
import 'package:admin_page/widget/revenue_info.dart';
import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class LandingScreen extends StatefulWidget {
  const LandingScreen({Key? key}) : super(key: key);

  @override
  State<LandingScreen> createState() => _LandingScreenState();
}

class _LandingScreenState extends State<LandingScreen> {

  final _beamerKey = GlobalKey<BeamerState>();
  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
            clipBehavior: Clip.antiAlias,
            margin: const EdgeInsets.fromLTRB(10.0, 10.0, 0.0, 10.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: SideNav(beamer: _beamerKey)),
        Expanded(
          child: Container(
            clipBehavior: Clip.antiAlias,
            margin: const EdgeInsets.all(10.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Beamer(
              key: _beamerKey,
              routerDelegate: BeamerDelegate(
                locationBuilder: RoutesLocationBuilder(
                  routes: {
                    '*': (context, state, data) =>  ProductScreen(),
                    '/order': (context, state, data) => const BeamPage(
                      key: ValueKey('order'),
                      type: BeamPageType.noTransition,
                      child: OrderScreen(),
                    ),
                    '/blog': (context, state, data) => const BeamPage(
                      key: ValueKey('blog'),
                      type: BeamPageType.noTransition,
                      child: BlogScreen(),
                    ),
                    '/user': (context, state, data) => const BeamPage(
                      key: ValueKey('user'),
                      type: BeamPageType.noTransition,
                      child: UserScreen(),
                    ),
                    '/request': (context, state, data) => const BeamPage(
                      key: ValueKey('request'),
                      type: BeamPageType.noTransition,
                      child: RequestScreen(),
                    ),
                  },
                ),
              ),
            ),
          ),
        ),
      ],
    );
  }
}

class SideNav extends StatefulWidget {
  final GlobalKey<BeamerState> beamer;
  const SideNav({Key? key, required this.beamer}) : super(key: key);

  @override
  State<SideNav> createState() => _SideNavState();
}

class _SideNavState extends State<SideNav> {
  int selected = -1;
  @override
  void initState() {
    super.initState();
  }

  List<String> navItems = [
    'Dashboard',
    'Order',
    'Blog',
    'User',
    'Request from User'
  ];
  List<String> navs = [
    '/dashboard',
    '/order',
    '/blog',
    '/user',
    '/request'
  ];

  @override
  Widget build(BuildContext context) {
    final path = (context.currentBeamLocation.state as BeamState).uri.path;
    print('path $path');
    if (path.contains('/dashboard')) {
      selected = 0;
    } else if (path.contains('/order')) {
      selected = 1;
    } else if (path.contains('/blog')) {
      selected = 2;
    } else if (path.contains('/user')) {
      selected = 3;
    } else if (path.contains('/request')) {
      selected = 4;
    }
    return Container(
      // color: Colors.grey[100],
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: navItems.map((navItem) {
          return AnimatedContainer(
            key: ValueKey(navItem),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10.0),
              color: selected == navItems.indexOf(navItem)
                  ? Colors.grey[850]
                  : Colors.white,
            ),
            duration: const Duration(
              milliseconds: 375,
            ),
            width: 120.0,
            margin: const EdgeInsets.symmetric(vertical: 15.0),
            child: Material(
              color: Colors.transparent,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              clipBehavior: Clip.antiAlias,
              child: InkWell(
                onTap: () {
                  setState(() {
                    widget.beamer.currentState?.routerDelegate
                        .beamToNamed(navs[navItems.indexOf(navItem)]);
                  });
                },
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Text(
                    navItem,
                    style: TextStyle(
                      color: selected == navItems.indexOf(navItem)
                          ? Colors.white
                          : Colors.grey[850],
                    ),
                  ),
                ),
              ),
            ),
          );
        }).toList(),
      ),
    );
  }
}
