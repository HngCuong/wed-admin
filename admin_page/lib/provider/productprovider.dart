import 'package:admin_page/api/apis.dart';
import 'package:admin_page/models/product.dart';
import 'package:flutter/cupertino.dart';

class ProductProvider with ChangeNotifier {
  List<Product> _results = [];
  int i = 0;
  void addData() {
      _results.clear();
      fetchProduct().then((value) =>
      {
        for (var item in value) {
          _results.add(item),
        },
        notifyListeners()
      });
      i++;
  }
  void callAgain(){
    if( i == 1 ){
      i - 1 ;
      _results.clear();
      notifyListeners();
    }
  }

  List<Product> get listProduct => _results ;

}