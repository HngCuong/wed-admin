import 'package:admin_page/api/apis.dart';
import 'package:admin_page/models/bill.dart';
import 'package:admin_page/models/request.dart';
import 'package:admin_page/models/useraccount.dart';
import 'package:flutter/cupertino.dart';


class RequestProvider with ChangeNotifier {
  List<RequestUser> _results = [];
  List<RequestUser> get item => _results;
  int i = 0;
  void addData() {
    _results.clear();
    fetchRequest().then((value) => {
      for (var item in value) {_results.add(item)},
      notifyListeners()
    }).then((value) => print(_results.length));
    i++;

  }
}

