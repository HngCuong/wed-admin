import 'dart:convert';
import 'dart:io';

import 'package:admin_page/models/bill.dart';
import 'package:admin_page/models/request.dart';
import 'package:admin_page/models/useraccount.dart';
import 'package:flutter/foundation.dart';

import '../models/product.dart';
import 'package:http/http.dart' as http;
String getProduct = "https://pfpt.azurewebsites.net/api/v1.0/product";
String getUser = "https://pfpt.azurewebsites.net/api/v1.0/customer";
String getRequest = "https://pfpt.azurewebsites.net/api/v1.0/request-customer/GetListBlog";
List<Product> parseProduct(String responseBody){
  var list = jsonDecode(responseBody) as Map<String, dynamic>;
  List<Product> product = List.empty();
  ProductResult food = ProductResult.fromJson(list);
  product = food.results!;
  print(product[0].name);
  return product;
}

Future<List<Product>> fetchProduct() async{
  final response = await http.get(Uri.parse(getProduct));
  if(response.statusCode == 200){
    return compute(parseProduct, response.body);
  }
  else{
    throw Exception('Request API Error');
  }
}

Future<List<Bill>> fetchBill() async{
  final response = await http.get(Uri.parse('https://pfpt.azurewebsites.net/api/v1.0/order'));
  if(response.statusCode == 200){
    return compute(parseBill, response.body);
  }
  else{
    throw Exception('Request API Error' + response.statusCode.toString());
  }
}

List<Bill> parseBill(String responseBody){
  var list = jsonDecode(responseBody) as Map<String, dynamic>;
  List<Bill> product = List.empty();
  Post food = Post.fromJson(list);
  product = food.resultss!;
  return product;
}



Future<bool> createProduct(ProductCreate a) async {
  Map<String, String> headers = {
    "Content-Type": "application/json",
  };
  final uri = Uri.parse("https://pfpt.azurewebsites.net/api/v1.0/product/CreateProduct");
  final response = await http.post(uri, body: jsonEncode(a.toJson()), headers: headers );
  if(response.statusCode == 200){
    print("Create Account Success");
    return true;
  }
  else{
    print(response.body);
    throw Exception('Request API Error' + response.statusCode.toString() );
  }
}

Future fetchStatusProduct(String a) async{
  Map<String, String> headers = {
    "Content-Type": "application/json",
  };
  final uri = Uri.parse("https://pfpt.azurewebsites.net/api/v1.0/product/DeleteProduct?productId="+a);
  final response = await http.delete(uri, body: jsonEncode({}),headers: headers);
  if(response.statusCode == 200){
    print(response.body);
  }
  else{
    throw Exception('Request API Error' + response.statusCode.toString());
  }

}
Future<bool> fetchBillStatus3(String a) async{

  print("Da chay fetch");
  print("https://pfpt.azurewebsites.net/api/v1.0/order/"+a+"?orderStatus=3");
  Map<String, String> headers = {
    "Content-Type": "application/json",
  };
  final uri = Uri.parse("https://pfpt.azurewebsites.net/api/v1.0/order/"+a+"?orderStatus=3");
  final response = await http.put(uri, body: jsonEncode({}),headers: headers);
  if(response.statusCode == 200){
    print(response.body);
  }
  else{
    throw Exception('Request API Error' + response.statusCode.toString());
  }
  return true;
}
Future<bool> fetchBillStatus2(String a) async{
  print("Da chay fetch");
  print("https://pfpt.azurewebsites.net/api/v1.0/order/"+a+"?orderStatus=2");
  Map<String, String> headers = {
    "Content-Type": "application/json",
  };
  final uri = Uri.parse("https://pfpt.azurewebsites.net/api/v1.0/order/"+a+"?orderStatus=2");
  final response = await http.put(uri, body: jsonEncode({}),headers: headers);
  if(response.statusCode == 200){
    print(response.body);
  }
  else{
    throw Exception('Request API Error' + response.statusCode.toString());
  }
  return true;
}
Future<bool> fetchBillStatus1(String a) async{
  print("Da chay fetch");
  print("https://localhost:44323/api/v1.0/order/"+a+"?orderStatus=1");
  Map<String, String> headers = {
    "Content-Type": "application/json",
  };
  final uri = Uri.parse("https://pfpt.azurewebsites.net/api/v1.0/order/"+a+"?orderStatus=1");
  final response = await http.put(uri, body: jsonEncode({}),headers: headers);
  if(response.statusCode == 200){
    print(response.body);
  }
  else{
    throw Exception('Request API Error' + response.statusCode.toString());
  }
  return true;
}
Future<bool> fetchBillStatus0(String a) async{
  print("Da chay fetch");
  print("https://localhost:44323/api/v1.0/order/"+a+"?orderStatus=0");
  Map<String, String> headers = {
    "Content-Type": "application/json",
  };
  final uri = Uri.parse("https://pfpt.azurewebsites.net/api/v1.0/order/"+a+"?orderStatus=0");
  final response = await http.put(uri, body: jsonEncode({}),headers: headers);
  if(response.statusCode == 200){
    print(response.body);
  }
  else{
    throw Exception('Request API Error' + response.statusCode.toString());
  }
  return true;
}

List<Acc> parseUser(String responseBody){
  var list = jsonDecode(responseBody) as Map<String, dynamic>;
  List<Acc> product = List.empty();
  User food = User.fromJson(list);
  product = food.results!;
  print("So Luong User" + product.length.toString());
  return product;
}

Future<List<Acc>> fetchUser() async{
  final response = await http.get(Uri.parse(getUser));
  if(response.statusCode == 200){
    return compute(parseUser, response.body);
  }
  else{
    throw Exception('Request API Error');
  }
}

List<RequestUser> parseRequest(String responseBody){
  var list = jsonDecode(responseBody) as Map<String, dynamic>;
  List<RequestUser> product = List.empty();
  Request food = Request.fromJson(list);
  product = food.results!;
  return product;
}

Future<List<RequestUser>> fetchRequest() async{
  final response = await http.get(Uri.parse(getRequest));
  if(response.statusCode == 200){
    return compute(parseRequest, response.body);
  }
  else{
    throw Exception('Request API Error');
  }
}