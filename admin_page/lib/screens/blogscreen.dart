import 'package:admin_page/widget/blogtable.dart';
import 'package:admin_page/widget/ordertable.dart';
import 'package:flutter/material.dart';
class BlogScreen extends StatefulWidget {
  const BlogScreen({super.key});
  @override
  State<BlogScreen> createState() => _CollectionState();
}
class _CollectionState extends State<BlogScreen> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(children: <Widget>[
          Container(
            child: Column(
              children: [
                BlogTable()
              ],
            ),
          )
        ]));
  }
}
