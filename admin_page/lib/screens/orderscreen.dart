import 'package:admin_page/widget/ordertable.dart';
import 'package:flutter/material.dart';
class OrderScreen extends StatefulWidget {
  const OrderScreen({super.key});
  @override
  State<OrderScreen> createState() => _CollectionState();
}
class _CollectionState extends State<OrderScreen> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(children: <Widget>[
          Container(
            child: Column(
              children: [
                OrderTable()
              ],
            ),
          )
        ]));
  }
}
