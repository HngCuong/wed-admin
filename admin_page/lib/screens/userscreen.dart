import 'package:admin_page/widget/blogtable.dart';
import 'package:admin_page/widget/ordertable.dart';
import 'package:admin_page/widget/usertable.dart';
import 'package:flutter/material.dart';
class UserScreen extends StatefulWidget {
  const UserScreen({super.key});
  @override
  State<UserScreen> createState() => _CollectionState();
}
class _CollectionState extends State<UserScreen> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(children: <Widget>[
          Container(
            child: Column(
              children: [
                UserTable()
              ],
            ),
          )
        ]));
  }
}
