import 'package:admin_page/widget/blogtable.dart';
import 'package:admin_page/widget/ordertable.dart';
import 'package:admin_page/widget/requesttable.dart';
import 'package:admin_page/widget/usertable.dart';
import 'package:flutter/material.dart';
class RequestScreen extends StatefulWidget {
  const RequestScreen({super.key});
  @override
  State<RequestScreen> createState() => _CollectionState();
}
class _CollectionState extends State<RequestScreen> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(children: <Widget>[
          Container(
            child: Column(
              children: [
                RequestTable()
              ],
            ),
          )
        ]));
  }
}
