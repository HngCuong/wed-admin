import 'package:admin_page/widget/available_drivers_table.dart';
import 'package:admin_page/widget/bar_chart.dart';
import 'package:admin_page/widget/head.dart';
import 'package:admin_page/widget/revenue_info.dart';
import 'package:flutter/material.dart';

class ProductScreen extends StatefulWidget {
  const ProductScreen({super.key});

  @override
  State<ProductScreen> createState() => _CollectionState();
}

class _CollectionState extends State<ProductScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: ListView(children: <Widget>[
      Container(
        child: Column(
          children: [
            Head(),
            SizedBox(
              height: 20,
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Row(
                  children: [
                    RevenueInfo(
                      title: "Toda's revenue",
                      amount: "0",
                    ),
                    RevenueInfo(
                      title: "Last 7 days",
                      amount: "0",
                    ),
                  ],
                ),
                SizedBox(
                  height: 30,
                ),
                Row(
                  children: [
                    RevenueInfo(
                      title: "Last 30 days",
                      amount: "0",
                    ),
                    RevenueInfo(
                      title: "Last 12 months",
                      amount: "0",
                    ),
                  ],
                ),
              ],
            ),
            SizedBox(
              height: 20,
            ),
            SizedBox(
                width: 600,
                height: 200,
                child: SimpleBarChart.withSampleData()),
            AvailableDriversTable()
          ],
        ),
      )
    ]));
  }
}
