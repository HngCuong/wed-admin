import 'package:admin_page/landing_screen.dart';
import 'package:admin_page/screens/blogscreen.dart';
import 'package:admin_page/screens/orderscreen.dart';
import 'package:admin_page/screens/requestscreen.dart';
import 'package:admin_page/screens/userscreen.dart';
import 'package:admin_page/widget/ordertable.dart';
import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';

class HomeLocation extends BeamLocation<BeamState> {
  HomeLocation(RouteInformation routeInformation) : super(routeInformation);
  @override
  List<BeamPage> buildPages(BuildContext context, BeamState state) {
    return [
      BeamPage(
          key: ValueKey('home-${DateTime.now()}'),
          child: const LandingScreen()),
    ];
  }

  @override
  List<Pattern> get pathPatterns => ['/*'];
}

class Order extends BeamLocation<BeamState> {
  Order(RouteInformation routeInformation)
      : super(routeInformation);
  @override
  List<BeamPage> buildPages(BuildContext context, BeamState state) {
    return [
      const BeamPage(
          key: ValueKey('order'),
          child: OrderScreen(),
          type: BeamPageType.fadeTransition),
    ];
  }

  @override
  List<Pattern> get pathPatterns => ['/order*'];
}

class Blog extends BeamLocation<BeamState> {
  Blog(RouteInformation routeInformation)
      : super(routeInformation);
  @override
  List<BeamPage> buildPages(BuildContext context, BeamState state) {
    return [
      const BeamPage(
          key: ValueKey('blog'),
          child: BlogScreen(),
          type: BeamPageType.fadeTransition),
    ];
  }

  @override
  List<Pattern> get pathPatterns => ['/blog*'];
}

class User extends BeamLocation<BeamState> {
  User(RouteInformation routeInformation)
      : super(routeInformation);
  @override
  List<BeamPage> buildPages(BuildContext context, BeamState state) {
    return [
      const BeamPage(
          key: ValueKey('user'),
          child: UserScreen(),
          type: BeamPageType.fadeTransition),
    ];
  }

  @override
  List<Pattern> get pathPatterns => ['/user*'];
}

class Request extends BeamLocation<BeamState> {
  Request(RouteInformation routeInformation)
      : super(routeInformation);
  @override
  List<BeamPage> buildPages(BuildContext context, BeamState state) {
    return [
      const BeamPage(
          key: ValueKey('request'),
          child: RequestScreen(),
          type: BeamPageType.fadeTransition),
    ];
  }

  @override
  List<Pattern> get pathPatterns => ['/request*'];
}